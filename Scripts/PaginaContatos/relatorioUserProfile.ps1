Add-PSSnapin Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue
#Exporta lista de usuarios do UserProfile e popula um csv
#Necessita ser spadmin2013
$SiteURL = "http://mytaesa.taesa.com.br"
$FileUrl = "c:\scripts\csv\user_profiles.csv"
#Pega contexto do user profile
$ServiceContext  = Get-SPServiceContext -site $SiteURL
$UPM = New-Object Microsoft.Office.Server.UserProfiles.UserProfileManager($ServiceContext)
$UserProfiles = $UPM.GetEnumerator()

#Cabecalho do CSV
"Account,DisplayName,WorkPhone,WorkEmail,JobTitle,Department" | out-file $FileUrl
		
foreach ($Profile in $UserProfiles)
{
    #regex que inclui no csv apenas matriculas no padrao novo
    if ( $Profile["AccountName"] -match '\w\w\w\w\w\\\d\d\d\d\d\d\d\d' -and $Profile["AccountName"].length -lt 15)
    { 

	"$($Profile["AccountName"]),$($Profile["PreferredName"]),$($Profile["WorkPhone"]),$($Profile["WorkEmail"]),$($Profile["Title"]),$($Profile["Department"])" | Out-File $FileUrl -Append
    }
}
