Add-PSSnapin Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue
#Abre o csv gerado pelo relatoriouserprofile.ps1 e popula a lista de Contatos
 
#le o csv
$CSVData = import-csv -path "C:\scripts\csv\user_profiles.csv"
 
$web = Get-SPWeb -identity "http://mytaesa.taesa.com.br"
 
#Obtem a lista
$list = $web.Lists["Contatos"]
 
#Percorre todos os itens do csv e popula as colunas correspondentes
foreach ($row in $CSVData)
 {
    
   $item = $list.Items.Add();
   $item["Nome"] = $row.DisplayName
   $item["Telefone"] = $row.WorkPhone
   $item["Email"] = $row.WorkEmail 
   $item["Cargo"] = $row.JobTitle  
   $item["Departamento"] = $row.Department
   $item.Update()
 }